package com.example.jishnu.passwordauthentication


import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class EnterPasswordActivity : AppCompatActivity() {
    internal lateinit var editText: EditText
    internal lateinit var button: Button
    internal var password: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_password)
        //load the password

        val settings = getSharedPreferences("PREFS", 0)
        password = settings.getString("Password", "")

        editText = findViewById(R.id.editText) as EditText
        button = findViewById(R.id.button) as Button

        button.setOnClickListener {
            val text = editText.text.toString()
            if (text == password) {
                //enter the app
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Toast.makeText(this@EnterPasswordActivity, "wrong password!!", Toast.LENGTH_SHORT).show()

            }
        }

    }
}
