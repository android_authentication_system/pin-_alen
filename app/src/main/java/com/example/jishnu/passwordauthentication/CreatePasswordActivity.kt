package com.example.jishnu.passwordauthentication


import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class CreatePasswordActivity : AppCompatActivity() {
    internal lateinit var editText1: EditText
    internal lateinit var editText2: EditText
    internal lateinit var  button: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_password)


        editText1 = findViewById(R.id.editText1) as EditText
        editText2 = findViewById(R.id.editText2) as EditText
        button = findViewById(R.id.button) as Button

        button.setOnClickListener {
            val text1 = editText1.text.toString()
            val text2 = editText2.text.toString()

            if (text1 == "" || text2 == "") {
                //there is no  password
                Toast.makeText(this@CreatePasswordActivity, "No Password entered", Toast.LENGTH_SHORT).show()


            } else {
                if (text1 == text2) {
                    //save the password
                    val settings = getSharedPreferences("PREFS", 0)
                    val editor = settings.edit()
                    editor.putString("Password", text1)
                    editor.apply()

                    //enter the app
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                    finish()


                } else {
                    //there is no match on the password
                    Toast.makeText(this@CreatePasswordActivity, "Password does not match", Toast.LENGTH_SHORT).show()

                }
            }
        }

    }
}