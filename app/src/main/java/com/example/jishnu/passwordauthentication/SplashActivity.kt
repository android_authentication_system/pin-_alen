package com.example.jishnu.passwordauthentication

import android.content.Intent
import android.content.SharedPreferences
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log


class SplashActivity : AppCompatActivity() {

    internal var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        //load the password

        val settings = getSharedPreferences("PREFS", 0)
        password = settings.getString("Password", "")
        // Log.v("password",password);


        val handler = Handler()
        handler.postDelayed({
            if (password === "") {
                //if there is no password
                val intent = Intent(applicationContext, CreatePasswordActivity::class.java)
                startActivity(intent)
                finish()

            } else {
                //if there is  a password
                val intent = Intent(applicationContext, EnterPasswordActivity::class.java)
                startActivity(intent)
                finish()

            }
        }, 2000)
    }
}
